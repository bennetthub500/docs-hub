# Salt Docs Hub

![The Salt Project](/docs/_static/img/SaltProject_altlogo_teal.png "The Salt Project")

Welcome to the Salt docs hub! This site is the working repository for the Salt
Docs Team. The docs hub includes:

- Information about contributing to the Salt docs and the Salt docs working
  group.
- Internal documentation about our documentation tech stack and policies.
- A sandbox for testing out work-in-progress documentation projects.

For more information about this repo, the Salt Docs tech stack, and contributing
to the Salt docs, view the website for this repository:
[Salt Docs Hub](https://saltstack.gitlab.io/open/docs/docs-hub/index.html)
