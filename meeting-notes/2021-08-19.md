# 2021-08-19 Meeting

## Attendees

- Derek Ardolf
- Bryce Larson
- Ken Jordan

## What we worked on this meeting

**Derek**

- Include templates for GitLab CI, for easy import docs publishing across all GitLab repos
  - Templates will assist in publishing to `docs.saltproject.io/*`

**Bryce**

- Hanging out

**Ken**

- Working on updating documentation around the schedule exec module: https://github.com/saltstack/salt/issues/60070

## Working group functionality

- Next meeting will be a "working" working group, where we use the time to work on tasks
  - Meetings, moving forward, are working meetups (at least until `salt-user-guide`
    and `salt-install-guide` are in MVP states and published)
- Paths for `docs.saltproject.io` publishing were logged in a previous working group for follow-up
  - [Meeting notes for 2021-07-08](https://github.com/saltstack/docs-hub/blob/master/meeting-notes/2021-07-08.md)

## Next steps

- Publish the `salt-user-guide` to `docs.saltproject.io/salt/user-guide/`
- Publish the `salt-install-guide` to `docs.saltproject.io/salt/install-guide/`
- See about implementing Algolia (or simply DuckDuckGo) for the current as-is docs portal

### Misc tasks

- Duplicate content from [VMware KB on Upgrading Salt](https://kb.vmware.com/s/article/50122319?lang=en_US&queryTerm=upgrading+your+salt+infrastructure) to install guide via [issue in `salt-install-guide`](https://gitlab.com/saltstack/open/docs/salt-user-guide/-/merge_requests/57)
  - Recent **Salt Air** mentioned that the `salt-master` doesn't need to be upgraded before minions
    - Upgrade documentation can say that we **recommend** upgrading the `salt-master` before upgrading minions.
- Add [Quick Guide to Vault Integration](https://web.archive.org/web/20210306232428/https://help.saltstack.com/hc/en-us/articles/360041140451-Quick-Guide-to-Vault-Integration) tutorial to `salt-user-guide`
- [Modify the main landing page of `salt-install-guide`](https://gitlab.com/saltstack/open/docs/salt-install-guide/-/issues/13)
- Confirm that Google tagging and analytics are stripped out where currently existing
- DuckDuckGo: Simple poc for docs landing `docs.saltproject.io`
