# 2022-01-11 meeting

## Attendees

- Alyssa Rock
- Melissa Strong
- Gary Geisen


## Meeting notes


### Announcements

- The next Docs working group meeting is January 25 at 3p.m. Mountain Time / 2p.m. Pacific Time / 22:00 UTC.
- The Docs working group repo `docs-hub` has moved from GitHub to GitLab: https://gitlab.com/saltstack/open/docs/docs-hub
- This repository also hosts the [Docs triage process](https://gitlab.com/saltstack/open/docs/docs-hub/-/blob/master/docs-triage-process.md) for those who are interested.


### Recap on Docs working group tasks for 2021

- As most of you are aware, Derek Ardolf (previous Docs working group captain) left VMware and Alyssa Rock is taking over as the new working group captain.
- As a result of this change, this group hasn't met for awhile. But we're excited to reboot the group!
- Summary of projects the group was working on before the reboot:
  - It seems like the key meeting where this group decided what to work on was the [2021-07-08 meeting](https://gitlab.com/saltstack/open/docs/docs-hub/-/blob/master/meeting-notes/2021-07-08.md).
  - The overall goal of the group's work was to make it easier to contribute to the Salt docs.
  - **Vale and the Salt Style Guide** - Gary was working on translating the Salt Style Guide into Vale linters. He hit a few snags. Between now and our next meeting, he'll refresh his memory on what those snags were. Alyssa will invite one of the SaltStack Config writers who has some experience with Vale to our next meeting.
  - **Salt User Guide** - We're so close to getting this published. Let's get it over the finish line. Alyssa will research what is preventing us from making this guide available to the community and report back next time to see if we can get this project unstuck.
  - **Salt Install Guide** - This project is on hold until Alyssa can finish her current project, but she'll likely work on it soon.
  - **Furo theme** Alyssa is currently researching whether to move forward with the Furo theme or the Material theme. She should have more to report at our next meeting about which theme we decided to move forward with.


### Salt Docs Roadmap

- Alyssa created a [Salt Docs Roadmap](https://saltdocs.prodcamp.com/) where community members and other stakeholders can view, request, and upvote big documentation improvements and initiatives.
- The goal of the Salt Docs Roadmap is to provide visibility to major documentation tasks. Alyssa wanted a way for community members and other stakeholders to see, upvote, and request major documentation improvements. In essence, this tool is a way of collecting and displaying that feedback to help the docs working group prioritize their work.
- The roadmap currently shows the improvement proposals that Alyssa gathered over several months talking with various Core team members, community members, and stakeholders.
- Last week, Alyssa met with internal stakeholders to identify the top priority for docs. That team of stakeholders came to a consensus that creating and implementing quality standards for module documentation (execution and state modules) would provide the most value to the community and Salt users.
- Gary mentioned that this project could and should be applied to the Salt Extensions project as well.


### Working group planning for the next release cycle

- We talked about going back to making these working meetings like they were when Derek was the working group captain. Maybe once we have our roadmap and plans in place we can start doing that.
- Gary wants to continue working with Vale for his next project.
- Melissa will work on familiarizing herself with the documentation contributing guides and will brainstorm what project she might want to work on first.


### Next docs working group meeting: January 25

Future:
- No future business items were tabled for discussion today.
