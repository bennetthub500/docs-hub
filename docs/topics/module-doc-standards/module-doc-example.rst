==================================================
Example of Salt style module documentation strings
==================================================

This document will provide an example of the Salt module documentation template
filled out correctly.

It should include a link to the other three guides:

* The template itself
* The contributing guidelines for writing module docs
* The module documentation navigation guidelines
