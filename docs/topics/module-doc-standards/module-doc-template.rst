==========================================
Salt module documentation strings template
==========================================

This document will provide a template that Salt users can copy into their
module documentation. It will have two sections: one for the module itself
and one for individual functions.

It should include a link to the other three guides:

* The template example
* The contributing guidelines for writing module docs
* The module documentation navigation guidelines
