.. _module-doc-standards-index:

==============================
Module documentation standards
==============================

Create clear benchmarks to measure module documentation quality, including
having clear examples and use cases. Then, audit the module documentation to
identify which modules need to be brought into compliance with these quality
standards.


Goals
=====

* Every Salt state and execution module meets a clear standard of excellence
  defined by the documentation and development teams.
* Each module should clearly explain its various intended use cases, any
  required and optional parameters with clear syntax and datatype examples, etc.
  The specific details of the quality standards will be defined as part of the
  work of this initiative.
* Clearly communicate the quality standards in a place that is visible to all
  community members and users.
* Apply these standards to all modules going forward. Then, audit and apply
  these standards to as many existing modules as possible, starting with the
  most commonly used modules.


Deliverables
============
Eventually, the goal will be to provide some guiding documents to users when
creating module documentation:

* A template that they can copy into their module documentation.
* An example of the template filled out correctly.
* A slightly more in-depth guide that explains how to use the template and what
  considerations to keep in mind when writing module documentation.
* Stretch goal: a guide to navigating and reading the module documentation.


Rough draft of proposed organization
====================================

.. list-table::
   :widths: 10 25 65
   :header-rows: 1
   :class: align-headers-left

   * - Required?
     - Element
     - Guidelines

   * -
     - TOC
     -  * Something to research as a stretch goal is whether the each module
          documentation page could auto-generate a TOC.
        * Might be possible if we upgrade Sphinx.

   * - Y
     - Source
     -  * Add a comment indicating the source file for the documentation (e.g.
          the file for the actual module)
        * Why? Sometimes users get confused about where they need to do the
          actual edit of the file.

   * - Y
     - Summary
     -  * Every module should include a description of what the module does.
        * The summary should include 1 or more practical, typical use cases and
          examples. Maybe a high level overview of the typical user stories for
          that module.
        * In a conversation with Gareth, we debated whether the examples should
          go into the summary for the whole module or whether the examples
          should go with the actual functions. For state modules, the summary
          should maybe be meatier. For execution modules, the functions should
          be meatier.
        * Optionally, it may also contain a brief description of classes and
          functions.
        * See `Google Style Guide - Modules <https://google.github.io/styleguide/pyguide.html#382-modules>`_
          for formatting guidelines.

   * - Y
     - Virtual name
     -  * You should include the virtual name of the module. For example, in
          the ``cmd.run`` module ``cmdmod`` is the Python equivalent, but the
          virtual name is a ``cmd``. The virtual name is what you would use to
          actually call the module and that is what the user actually needs.
        * Question for dev team: is there a way to programmatically pull this
          out?

   * - Y
     - Links to related state or virtual modules
     -  * A lot of new users are confused by the module organization. Let's
          help them out by providing links to relevant modules.
        * For execution modules, the module should link back to its counterpart
          state module.
        * For state modules, the module should link to its virtual module list.
          For example, `salt.modules.pkg <https://docs.saltproject.io/en/latest/ref/modules/all/salt.modules.pkg.html#module-salt.modules.pkg>`_.
        * Side project: We should consider creating a guide for how the modules are organized.

   * - N
     - Dependencies
     -  * The module should list any dependencies that need to be installed in
          order for it to function correctly.
        * The dependencies should list the pip installation commands. Highly
          recommended because it could depend based on the operating system.
        * The `impi module <https://docs.saltproject.io/en/latest/ref/modules/all/salt.modules.ipmi.html#module-salt.modules.ipmi>`_
          shows a good example of listing dependencies.
        * Should they say "dependencies" or "depends"? We could say
          "dependencies" and make that a preference, but not a strong
          requirement.
        * Also need to list conditional/optional dependencies for kwargs or
          things like that.
        * Maybe put the function-specific dependencies with the function, but
          list the ones that do in the top level dependency.

   * - Y
     - Configuration
     -  * The module should explain how to configure the module and what the
          different configuration options do.
        * If the module needs a configuration addition, it needs to be required
          but not all modules will need this section. Should list which files
          the configuration options need to be added to.
        * Link out to the configuration documentation.
        * Some modules can handle pillar communication and, if so, that should
          be noted in the module.
        * Also needs a validation step and more detail about options.

   * - N
     - Notes or warnings
     -  * Any important information about versioning or warnings should be
          indicated.
        * Possibly putting it at the top might be best practice, but then that
          makes the top the list of all the warnings and that's a problem too.
          Consider placement and think about what's really important. Don't
          just continually keep adding to it.
        * Sometimes they're more important to a specific function.
        * If the documentation is well-written, it shouldn't need notes. But
          warnings are important.
        * Warnings are justified when it's going to introduce a breaking
          change, cause functionality issues, or if there's an upcoming
          deprecation warning.

   * - Y
     - Functions
     -  * This is where the detailed use cases can go.
        * Patrick mentioned some good possible indicators for when a
          module/function is complex (and therefore needs more use cases) has
          to do with its parameters. For example, if you've just got some
          simple positional args, you maybe don't need a lot of use cases. But
          maybe for every, say, 5 kwargs, he would expect to see at least one
          use case.

   * - Y
     - Parameters
     -  * For each parameter, you should indicate its data type (string, dict,
          etc.) the same way you indicate what part of speech a word is in the
          dictionary.
        * Returns should be part of the parameters.

   * - Y
     - Return data / example output
     -  * Feedback from Docs WG: don't just include CLI output. The examples
          should feature both CLI examples and Jinja examples because the data
          structure matters.
        * Boucha felt that the request for Jinja output would refer more to
          state modules. The output should be what the state module accepts.
          However, if there is a common Jinja use case or example you could put
          that in. Otherwise, it might not apply to everyone.
        * We would put the example of how to call it and an example of return
          data.
        * The first example should be a CLI call and with the corresponding
          output. And the second example (or more) should be a Jinja call and
          the sample output from that.
        * Make sure there is some sort of validation step. Here is correct and
          here is incorrect. Maybe some type of checklist to make sure you
          cover all the right cases to make sure it's working correctly.

   * -
     - Code comment
     -  * It would be helpful to have clear code comments that explain the
          property and what it does.
        * This is covered by the items above it.

   * -
     - Metadata - health checks
     -  * Would it help to indicate module maturity and core support as a
          metadata tag on the module?
        * Possibly fetch the most recent commit date from the module or its
          activity to show what kind of developer support that module is
          receiving; most recent test status.
        * This might be out of scope.


Relevant links
==============

* The `impi module <https://docs.saltproject.io/en/latest/ref/modules/all/salt.modules.ipmi.html#module-salt.modules.ipmi>`_ impi module shows a good example of listing dependencies.
* The `cdmod module <https://docs.saltproject.io/en/latest/ref/modules/all/salt.modules.cmdmod.html#module-salt.modules.cmdmod>`_
* `Salt pillar module <https://docs.saltproject.io/en/latest/ref/pillar/all/salt.pillar.mysql.html#module-salt.pillar.mysql>`_ gives maturity metadata
* `Pkg state module <https://docs.saltproject.io/en/latest/ref/states/all/salt.states.pkg.html#module-salt.states.pkg>`_ is helpful to reference
* `Pkg execution modules <https://docs.saltproject.io/en/latest/ref/modules/all/salt.modules.pkg.html#module-salt.modules.pkg>`_ for reference
* `Google Python docstrings style guide <https://google.github.io/styleguide/pyguide.html#382-modules>`_
* `Example Google Python docstrings <https://www.sphinx-doc.org/en/master/usage/extensions/example_google.html>`_


Related conversation with a community member
============================================

**On Feb 22 msmith writes:**

i'd like to know if there's an existing page that describes how to convert
the module syntax in the docs to working examples in a generic way. this is
so that we can point people to this page when they can't find an example that
covers their exact combination of arguments, and thus the use case driving it

**Alyssa writes:**

Fortunately I'm currently working on a project where we will standardize and
create quality benchmarks for the module documentation.

If I understand your request correctly, you have ideas about how we can make
sure that the module syntax has clear examples of how to use a variety of
combination of arguments. I can definitely make sure that is included in
these new guidelines.

I'm hoping that I'll have the rough draft of these guidelines ready for
community comment within a few sprint cycles. Would you be at all interested
in reviewing and commenting on these guidelines when they're ready?

**msmith writes:**

i'm thinking of a generic page that relates to how to interpret the rest of
the docs, as it's unlikely to ever cover every possible use case as an
example

many users aren't technical enough to be able to understand what a * means in
the argument list, unless we have a page that explains it

most of what i do in this slack has been this very interpretation for people
who don't understand the docs, and it would mean i could get on with my day
job if there was a page that clearly explained this

**Alyssa writes:**

Ah, yes, so a generic guide that explains how to read some of the common
elements in modules, such as wildcards * ?


**msmith writes:**

indeed, with examples for how to translate those from function calls into
both cli and jinja

many queries come in where it's not clear to them if an argument is
positional, named, a list, or a dict, and they've picked the wrong one


Templates
=========

.. toctree::
   :maxdepth: 1
   :glob:

   *
