.. div:: landing-title-bottom-margin

    .. grid::
        :reverse:
        :gutter: 2 3 3 3
        :margin: 4 4 1 2

        .. grid-item::
            :columns: 12 3 3 3
            :class: sd-m-auto sd-animate-grow50-rot20

            .. image:: /_static/img/docs-hub-small.png
              :width: 125
              :alt: Salt Project docs hub logo

        .. grid-item::
            :columns: 12 9 9 9
            :child-align: center
            :class: sd-text-white sd-fs-2 sd-text-center

            The Salt Docs Hub

            .. button-link:: https://saltdocs.prodcamp.com/
               :color: light
               :align: center
               :outline:

                Go to the Salt docs product roadmap

Welcome to the Salt docs hub! This site is the working repository for the Salt
Docs Team. The docs hub includes:

* Information about contributing to the Salt docs.
* Internal documentation about our documentation tech stack and policies.
* A sandbox for testing out work-in-progress documentation projects.


The Salt doc sets
=================
The Salt documentation ecosystem consists of the following doc sets and
repositories:

.. list-table::
  :widths: 20 40 40
  :header-rows: 1
  :stub-columns: 1

  * - Title
    - Description
    - Technical details

  * - Core Salt docs
    -  * The original Salt docs for the project.
       * Primarily contains conceptual information and reference information
         about Salt.
       * Also contains release notes.
    -  * **On the web:** `Core Salt docs <https://docs.saltproject.io/en/latest/contents.html>`_
       * **Repo:** `Salt (in the doc folder) <https://github.com/saltstack/salt>`_
       * **Tech stack:** GitHub, Sphinx, rst, custom theme, hosting unknown
       * **Maintainers:** The core Salt team
       * **Status:** :octicon:`alert-fill;1em;sd-text-danger` Need to archive
         all but release notes to the Salt Reference Docs

  * - Get Started
    -  * An outdated guide and tutorial designed to introduce users to Salt
    -  * **On the web:** `Get Started site <https://docs.saltproject.io/en/getstarted/>`_
       * **Repo:** `Get Started <https://gitlab.com/saltstack/open/docs/get-started>`_
       * **Tech stack:** GitLab, Acrylamid, Python 2, hosting unknown
       * **Maintainers:** No one
       * **Status:** :octicon:`alert-fill;1em;sd-text-danger` Need to archive

  * - Module documentation
    -  * Provides the necessary information users need for each state and
         execution module.
       * The module documentation is arguably the most important and valuable
         documentation set.
    -  * **On the web:** `All Salt modules <https://docs.saltproject.io/en/latest/py-modindex.html>`_
       * **Repo:** `Salt <https://github.com/saltstack/salt>`_
       * **Tech stack:** GitHub, Sphinx, Python 3, rst, custom theme, hosted unknown
       * **Maintainers:** The core Salt team
       * **Status:** :octicon:`check-circle-fill;1em;sd-text-success` Active,
         but needs significant improvement; see
         `Create and implement quality standards for module documentation <https://saltdocs.prodcamp.com/2>`_

  * - Salt Branding Guide
    -  * Provides branding guidelines for the Salt Project.
       * Includes official icons and a guide for the Salt Project color scheme,
         font, etc.
    -  * **On the web:** Not published
       * **Repo:** `Salt Branding <https://gitlab.com/saltstack/open/salt-branding-guide>`_
       * **Tech stack:** GitLab
       * **Maintainers:** Salt Project community manager, Salt docs team
       * **Status:** :octicon:`check-circle-fill;1em;sd-text-success` Active

  * - Salt Docs Hub

      .. image:: /_static/img/docs-hub-small.png
         :width: 50
         :align: center
    -  * Information about contributing to the Salt docs.
       * Internal documentation about our documentation tech stack and policies.
       * A sandbox for testing out work-in-progress documentation projects.
    -  * **On the web:** `Docs Hub <https://saltstack.gitlab.io/open/docs/docs-hub/>`_
       * **Repo:** `Docs Hub (GitLab) <https://gitlab.com/saltstack/open/docs/docs-hub>`_
       * **Tech stack:** GitLab, Sphinx, rst, Furo theme, hosted on GitLab Pages
       * **Maintainers:** The Salt docs team
       * **Status:** :octicon:`check-circle-fill;1em;sd-text-success` Active

  * - Salt Reference Docs

      .. image:: /_static/img/salt-reference-docs-small.png
         :width: 50
         :align: center
    -  * Eventually the Salt docs working group hopes to migrate the core Salt
         docs here.
       * Putting them here will make it easier to contribute to the docs, will
         give these docs all the awesome upgrades of our standard docs tech
         stack that the custom theme doesn't use, and will allow us to archive
         and upgrade stale docs.
    -  * **On the web:** Not published yet
       * **Repo:** `Salt Reference Docs (GitLab) <https://gitlab.com/saltstack/open/docs/salt-reference-docs>`_
       * **Tech stack:** GitLab, Sphinx, rst, Furo theme, hosted on GitLab Pages
       * **Maintainers:** The Salt docs team
       * **Status:** :octicon:`tools;1em;sd-text-success` In progress; at the
         very early stages


  * - Salt Install Guide

      .. image:: /_static/img/salt-install-guide-small.png
         :width: 50
         :align: center
    -  * Will eventually replace the install guide content on repo.saltproject.io.
       * Includes install instructions for Salt for every supported operating
         system.
       * Also includes information about the supported operating systems, product
         version support lifecycle, supported versions of Python.
    -  * **On the web:** `Salt Install Guide <https://docs.saltproject.io/salt/install-guide/en/latest/>`_
       * **Repo:** `Salt Install Guide (GitLab) <https://gitlab.com/saltstack/open/docs/salt-install-guide>`_
       * **Tech stack:** GitLab, Sphinx, rst, Furo theme, hosted on GitLab Pages
       * **Maintainers:** The Salt docs team
       * **Status:** :octicon:`tools;1em;sd-text-success` In progress; will be
         published with the 3005 Salt release

  * - Salt Success Stories

      .. image:: /_static/img/salt-success-stories-small.png
         :width: 50
         :align: center
    -  * Features stories from real users about how they effectively use Salt to
         solve practical system administration problems in a variety of contexts.
       * The end goal is to inspire users to see what they can really do with
         Salt.
    -  * **On the web:** Not published yet
       * **Repo:** `Salt Success Stories (GitLab) <https://gitlab.com/saltstack/open/docs/salt-success-stories>`_
       * **Tech stack:** GitLab, Sphinx, rst, Furo theme, hosted on GitLab Pages
       * **Maintainers:** The Salt docs team
       * **Status:** :octicon:`tools;1em;sd-text-success` In progress; at the
         very early stages

  * - Salt User Guide

      .. image:: /_static/img/salt-user-guide-small.png
         :width: 50
         :align: center
    -  * Includes conceptual information about using Salt.
    -  * **On the web:** `Salt User Guide <https://docs.saltproject.io/salt/user-guide/en/latest/>`_
       * **Repo:** `Salt User Guide (GitLab) <https://gitlab.com/saltstack/open/docs/salt-user-guide>`_
       * **Tech stack:** GitLab, Sphinx, rst, Furo theme, hosted on GitLab Pages
       * **Maintainers:** The Salt docs team
       * **Status:** :octicon:`check-circle-fill;1em;sd-text-success` Active

  * - Private repositories
    - The Salt Project hosts a few documentation-related repositories that are
      private. For example, the repo that contains the docs landing page is
      private. Only VMware employees with a legitimate business purpose for
      accessing those repositories are allowed to view or contribute to those
      repositories.
    - These repositories will not be listed here.


Salt docs dependencies
======================
VMware, Inc. is the primary corporate sponsor for the Salt Project. They have
several other documentation sets that are dependent on the Salt docs:

* `SaltStack Config <https://docs.vmware.com/en/VMware-vRealize-Automation-SaltStack-Config/index.html>`_
* `vRealize Automation <https://docs.vmware.com/en/vRealize-Automation/index.html>`_
* Idem

These doc sets are maintained by other teams besides the Salt docs team,
although these teams are in communication with each other as stakeholders.
