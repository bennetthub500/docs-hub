.. _docs-triage-process:

========================
Salt docs triage process
========================

The Salt Documentation Working Group is committed to reviewing new documentation
issues that have been opened against any of the repositories they manage,
including issues from the Open Salt repository that have been assigned to the
docs team. Regular triage meetings ensure that the Documentation team responds
to tickets effectively and within a reasonable timeframe.


Triage quick start
==================
The documentation team holds a triage meeting for an hour once a week. This
meeting is currently closed to the community. In this meeting, the Docs team
reviews any new tickets that have been opened against our repos. Our SLA for
responding to a ticket when it is opened is generally about one week, although
that may be delayed by holidays or vacations.


Triage meeting agenda
=====================
Triage meetings typically follow these steps:

1. Review and triage all newly opened tickets that were opened in all
   docs-impacting repositories (the process differs for GitHub vs. GitLab).

.. list-table::
  :widths: 40 60
  :header-rows: 1

  * - Where
    - What

  * - GitLab
    - `All open issues in all repos <https://gitlab.com/groups/saltstack/open/docs/-/issues?state=opened>`_

  * - GitHub - Salt
    - `Alyssa's Salt issues <https://github.com/saltstack/salt/issues/assigned/barbaricyawps>`_

  * - GitHub - Salt
    - `By documentation label (Salt) <https://github.com/saltstack/salt/issues?q=is%3Aopen+is%3Aissue+label%3ADocumentation>`_

  * - GitHub - Bootstrap
    - `Alyssa's Bootstrap issues <https://github.com/saltstack/salt-bootstrap/issues/assigned/barbaricyawps>`_

  * - GitHub - Bootstrap
    - `By documentation label (Bootstrap) <https://github.com/saltstack/salt-bootstrap/issues?q=is%3Aopen+is%3Aissue+label%3ADocumentation>`_

2. Review all new tickets, assign appropriate labels, and ask for more
   clarification from the individual who opened the ticket as needed. See
   `Purpose and overview of triage`_ for information about the criteria to use
   when evaluating tickets and applying labels.

3. After reviewing new tickets, search for any issues that were tabled for
   discussion last time. These tickets were usually assigned this label as a
   reminder to follow up on it in a future triage meeting.

   * In GitHub, these are issues with the **Documentation** and **info-needed** labels.
   * In GitLab, these are issues with the **Triage needed (docs)** label.

4. If time allows, groom the backlog.

   * Grooming means reviewing the oldest tickets in our backlog to determine if
     they need to be closed or archived.
   * Our policy for closing tickets is whether the ticket is actionable or not.
     If the ticket is not actionable, it means more information is needed from
     the individual who opened the ticket. If they don't respond for requests for
     more information within a week, the ticket may be closed. We typically give
     a one-week notice before closing to give the person who opened the ticket a
     chance to respond.


Purpose and overview of triage
==============================
The goal of triage is to regularly review newly-opened documentation issues
(also sometimes referred to as “tickets”) and prepare them for future work. The
purpose of triage is not necessarily to resolve tickets. The purpose is to get
tickets into an actionable state and to add necessary metadata that will help
the team make important decisions about when to work on a given issue.

When the documentation team evaluates a ticket, they need to determine:

* **Severity level** - How urgent or important is this issue? Is it a high risk
  issue?
* **Time estimate** - How long will it take to resolve the issue and how much
  work will be involved?
* **General docs element, topic, or type (optional)** - Does the issue impact a
  specific docs element, such as the API docs or Sphinx? Does it impact a
  specific topic, such as the Downloads page or LDAP? Is the issue related to a
  larger type of issue, such as style guide issues or Search and navigation?
* **Team dependency** - Does the issue need the entire Documentation team’s
  attention? Does the issue need support from additional teams like QA, UX, or
  SRE?
* **Correct repo** - Was the issue opened in the same repository in which the
  fix for the issue will be submitted?
* **Clarity needed?** - Is it clear from the issue’s description and comments
  what work needs to be done to resolve this issue? Is more clarification needed
  from the individual who opened the ticket?

.. Note::
    If an issue can’t be assigned a severity level or time estimate, add a label
    to indicate this ticket must be triaged after more information has been
    gathered. For GitLab: add the **Triage needed (docs)** label. For GitHub:
    add the **info-needed** label.


Labels
======
The documentation team will then assign metadata to the issue in the form of
labels that captures this information. If the issue needs further clarity, the
team will follow up with the individual who opened the issue for more
information.


Severity level
--------------

The documentation team currently uses the following severity levels to designate
a ticket’s impact and importance:

.. list-table::
  :widths: 20 40 40
  :header-rows: 1

  * - Label
    - Description
    - Examples

  * - Critical
    - The software has a critical problem that needs immediate attention and
      supporting documentation is needed.
    -   * Security vulnerabilities
        * Documentation or website functionality failures

  * - High
    - The documentation is in a state that is actively impacting a large number
      of users and affects their ability to use the software effectively or
      access the documentation. In a word: urgent.
    -   * Inaccurate documentation
        * Missing documentation
        * Known issues in the software

  * - Medium
    - The documentation is still in a usable state but could be misleading or
      confusing to users.
    -   * Formatting issues
        * Grammar or punctuation errors
        * Improper terminology

  * - Low
    - The product or documentation is in a usable state but could be improved.
    -   * Style guide inconsistencies
        * Cosmetic fixes

.. Note::
    In GitLab, these are scoped labels by Severity. In GitHub, the labels are
    prepended by **severity**, such as **severity-critical**, **severity-high**,
    etc.


Time estimate
-------------
The documentation team currently uses the following time estimates to signify
our best guess about how long an issue will take to resolve:

.. list-table::
  :widths: 20 40 40
  :header-rows: 1

  * - Label
    - Description
    - Examples

  * - Long-term
    - A major task that could take a month or more and is usually reserved for
      full documentation initiatives. It will involve collaborating with
      cross-functional teams and may involve planning sessions. The task might
      require approval from upper management because it will involve resources.
    -   * Epics
        * Major back-end or front-end changes
        * Creating full new doc sets comprising several topics
        * Migrations
        * Major reworks of key content

  * - Sprint
    - A fairly major task that could take a 1-3 week time period to complete.
      These tasks might involve interviewing subject matter experts or
      independent research.
    -   * Documentation for a feature release or that satisfies a user story
        * Improvements to existing documentation

  * - Single-day
    - A task that could likely be completed in 1-2 days of dedicated time. (It
      might take up to a week after it gets through the review process.)
    -   * Reworking a single topic
        * Updating a style guide issue in a set of topics or a small doc set

  * - Quick fix
    - A task that could take a few minutes, an hour, or up to half a day to
      complete. No subject matter reviews needed. Low-hanging fruit.
    -   * Fixing a typo
        * Fixing a minor formatting error
        * Adding a few lines of text


.. Note::
    In GitLab, these are scoped labels by Time Estimate. In GitHub, the labels
    are prepended by **time-estimate**, such as **time-estimate-long-term**,
    **time-estimate-sprint**, etc.


General docs element, topic, or type (optional)
-----------------------------------------------
If needed, assign the issue a label that describes the specific docs element it
impacts.

In Open Salt:

.. list-table::
  :widths: 25 75
  :header-rows: 1

  * - Label
    - Description

  * - `docstring-update <https://github.com/saltstack/salt/labels/docstring-update>`_
    - For module documentation.

  * - `doc-rst-update <https://github.com/saltstack/salt/labels/doc-rst-update>`_
    - For Core Salt topic documentation.

  * - `community-docs <https://github.com/saltstack/salt/labels/community-docs>`_
    - For Community docs like the contributing guide, Code of Conduct, etc.

  * - `salt-install-guide <https://github.com/saltstack/salt/labels/salt-install-guide>`_
    - For issues that affect repo.saltproject.io or the `Salt Install Guide <https://gitlab.com/saltstack/open/docs/salt-install-guide>`_

  * - `help-wanted <https://github.com/saltstack/salt/labels/help-wanted>`_
    - For issues that would be good for community members to work on.


Team
----
If the issue needs support from an additional team, such as QA or SRE, add those
labels to the issue. If the issue will require support from the full
Documentation team or needs to be labeled as a docs ticket for visibility in
other repos, add the Documentation team label.


Correct repo
------------
Ideally, issues should be opened in the repository where the merge request
and/or pull request for that work will be done. If the issue was not opened in
the correct repo, it should be moved to that repo.


Clarity needed
--------------
In some cases, it may not be clear from the issue’s description and comments
what work needs to be done to resolve this issue. The team might also not be
able to assign a severity or complexity level until more information is
gathered.

When that occurs, the team should:

1. Add a comment requesting clarification from the individual who opened the issue.

2. Add the appropriate labels.

   * In GitHub, these are issues with the **Documentation** and **info-needed**
     labels.
   * In GitLab, these are issues with the **Triage needed (docs)** label.
